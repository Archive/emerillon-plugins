/*
 * Copyright (C) 2008 Pierre-Luc Beaudoin <pierre-luc@pierlux.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef BIXI_MARKER_H
#define BIXI_MARKER_H

#include <champlain/champlain.h>
#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define BIXI_TYPE_MARKER     (bixi_marker_get_type())
#define BIXI_MARKER(obj)     (G_TYPE_CHECK_INSTANCE_CAST((obj), BIXI_TYPE_MARKER, BixiMarker))
#define BIXI_MARKER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),  BIXI_TYPE_MARKER, BixiMarkerClass))
#define BIXI_IS_MARKER(obj)  (G_TYPE_CHECK_INSTANCE_TYPE((obj), BIXI_TYPE_MARKER))
#define BIXI_IS_MARKER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),  BIXI_TYPE_MARKER))
#define BIXI_MARKER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),  BIXI_TYPE_MARKER, BixiMarkerClass))

typedef struct _BixiMarkerPrivate BixiMarkerPrivate;

typedef struct _BixiMarker BixiMarker;
typedef struct _BixiMarkerClass BixiMarkerClass;

struct _BixiMarker
{
  ChamplainMarker base;

  BixiMarkerPrivate *priv;
};

struct _BixiMarkerClass
{
  ChamplainMarkerClass parent_class;

  void (* draw_marker) (BixiMarker *marker);
};

GType bixi_marker_get_type (void);

ClutterActor *bixi_marker_new (void);

void bixi_marker_set_value (BixiMarker *marker, guint value);

G_END_DECLS

#endif
