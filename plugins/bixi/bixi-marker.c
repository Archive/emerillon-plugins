/*
 * Copyright (C) 2009 Novopia Solutions Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "bixi-marker.h"

#include <clutter/clutter.h>
#include <glib.h>
#include <glib-object.h>
#include <cairo.h>
#include <math.h>
#include <string.h>


struct _BixiMarkerPrivate
{
  ClutterActor *shadow;
  ClutterActor *background;
  ClutterActor *text_actor;
  gchar *text;

  gint value;
};

G_DEFINE_TYPE (BixiMarker, bixi_marker, CHAMPLAIN_TYPE_MARKER);

#define BIXI_MARKER_GET_PRIVATE(obj)    (G_TYPE_INSTANCE_GET_PRIVATE((obj), BIXI_TYPE_MARKER, BixiMarkerPrivate))

static void draw_marker (ChamplainMarker *marker);

static void
bixi_marker_finalize (GObject *object)
{
  BixiMarker *marker = BIXI_MARKER (object);
  BixiMarkerPrivate *priv = marker->priv;

  G_OBJECT_CLASS (bixi_marker_parent_class)->finalize (object);
}

static void
bixi_marker_class_init (BixiMarkerClass *bixiClass)
{
  g_type_class_add_private (bixiClass, sizeof (BixiMarkerPrivate));

  GObjectClass *object_class = G_OBJECT_CLASS (bixiClass);
  object_class->finalize = bixi_marker_finalize;

  ChamplainMarkerClass *markerClass = CHAMPLAIN_MARKER_CLASS (bixiClass);
  markerClass->draw_marker = draw_marker;
}

#define PADDING (RADIUS / 2)

static void
draw_box (cairo_t *cr,
   gint radius)
{
      cairo_arc (cr, radius, radius, radius -1, 0, 2 * M_PI);
      cairo_close_path (cr);
}

static void
draw_shadow (BixiMarker *marker,
   gint radius)
{
  BixiMarkerPrivate *priv = marker->priv;
  ClutterActor *shadow = NULL;
  cairo_t *cr;

  shadow = clutter_cairo_texture_new (radius * 2, radius * 2);
  cr = clutter_cairo_texture_create (CLUTTER_CAIRO_TEXTURE (shadow));

  draw_box (cr, radius);

  cairo_set_source_rgba (cr, 0, 0, 0, 0.25);
  cairo_fill (cr);

  cairo_destroy (cr);

  clutter_actor_set_position (shadow, 3, 3);

  clutter_container_add_actor (CLUTTER_CONTAINER (marker), shadow);

  if (priv->shadow != NULL)
    {
      clutter_container_remove_actor (CLUTTER_CONTAINER (marker),
          priv->shadow);
      g_object_unref (priv->shadow);
    }

  priv->shadow = g_object_ref (shadow);
}

static ClutterColor default_text_color = {0xff, 0xff, 0xff, 0xee};

static void
draw_background (BixiMarker *marker,
    gint radius)
{
  BixiMarkerPrivate *priv = marker->priv;
  ClutterActor *bg = NULL;
  ClutterColor color;
  ClutterColor darker_color;
  gboolean highlighted = FALSE;
  guint line_width = 1;
  cairo_t *cr;

  bg = clutter_cairo_texture_new (radius * 2, radius * 2);
  cr = clutter_cairo_texture_create (CLUTTER_CAIRO_TEXTURE (bg));
  color = *champlain_marker_get_color (CHAMPLAIN_MARKER (marker));

  if (priv->value == 0)
    color.alpha = 128;

  g_object_get (marker, "highlighted", &highlighted, NULL);
  if (highlighted)
    {
#if CHAMPLAIN_CHECK_VERSION(0, 4, 1)
      color = *champlain_marker_get_highlight_color ();
#else
      line_width *= 3;
#endif
    }

  clutter_color_darken (&color, &darker_color);
  draw_box (cr, radius);

  cairo_set_source_rgba (cr,
      color.red / 255.0,
      color.green / 255.0,
      color.blue / 255.0,
      color.alpha / 255.0);
  cairo_fill_preserve (cr);

  cairo_set_line_width (cr, line_width);
  cairo_set_source_rgba (cr,
      darker_color.red / 255.0,
      darker_color.green / 255.0,
      darker_color.blue / 255.0,
      darker_color.alpha / 255.0);
  cairo_stroke (cr);
  cairo_destroy (cr);

  clutter_container_add_actor (CLUTTER_CONTAINER (marker), bg);

  if (priv->background != NULL)
    {
      clutter_container_remove_actor (CLUTTER_CONTAINER (marker),
          priv->background);
      g_object_unref (priv->background);
    }

  priv->background = g_object_ref (bg);
}

static void
draw_marker (ChamplainMarker *marker)
{
  BixiMarkerPrivate *priv = BIXI_MARKER (marker)->priv;
  ClutterText *label;
  gfloat width = 0, height = 0;
  gint radius = 10 + priv->value / 3;

  g_free (priv->text);
  priv->text = g_strdup_printf ("%u", priv->value);

  if (priv->text_actor == NULL)
    {
      priv->text_actor = clutter_text_new_with_text ("Sans 11", priv->text);
      g_object_ref (priv->text_actor); }

  label = CLUTTER_TEXT (priv->text_actor); clutter_text_set_font_name (label,
      "Sans 11"); clutter_text_set_text (label, priv->text);
  clutter_actor_get_size (CLUTTER_ACTOR (label), &width, &height);
  clutter_actor_set_position (CLUTTER_ACTOR (label), radius - width / 2, radius
      - height / 2);

  clutter_text_set_color (CLUTTER_TEXT (priv->text_actor), &default_text_color);
  if (clutter_actor_get_parent (priv->text_actor) == NULL)
    clutter_container_add_actor (CLUTTER_CONTAINER (marker), priv->text_actor);

  draw_shadow (BIXI_MARKER (marker), radius); draw_background (BIXI_MARKER
      (marker), radius);

  clutter_actor_raise (priv->text_actor, priv->background);
  clutter_actor_set_anchor_point (CLUTTER_ACTOR (marker), radius, radius); }

void
bixi_marker_set_value (BixiMarker *marker,
                       guint value)
{
  BixiMarkerPrivate *priv = BIXI_MARKER_GET_PRIVATE (marker);

  priv->value = value;
}

static void
bixi_marker_init (BixiMarker *marker)
{
  BixiMarkerPrivate *priv = BIXI_MARKER_GET_PRIVATE (marker); marker->priv = priv;

  priv->value = 0;
  priv->text = NULL;
}

ClutterActor *
bixi_marker_new (void)
{
  return CLUTTER_ACTOR (g_object_new (BIXI_TYPE_MARKER, NULL));
}

