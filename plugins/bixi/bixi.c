/*
 * Copyright (C) 2009 Novopia Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include "bixi.h"
#include "bixi-marker.h"
#include "emerillon/emerillon.h"

#include <locale.h>
#include <glib/gi18n.h>
#include <rest/rest-proxy.h>
#include <rest/rest-proxy-call.h>
#include <rest/rest-xml-parser.h>

G_DEFINE_TYPE (BixiPlugin, bixi_plugin, ETHOS_TYPE_PLUGIN)

enum {
  COL_ID,
  COL_NAME,
  COL_TERMINAL_NAME,
  COL_LAT,
  COL_LON,
  COL_INSTALLED,
  COL_LOCKED,
  COL_TEMPORARY,
  COL_NB_BIKES,
  COL_NB_BIKES_S,
  COL_NB_EMPTY_DOCKS,
  COL_NB_EMPTY_DOCKS_S,
  COL_MARKER,
  COL_COUNT
};

enum {
  COL2_ICON,
  COL2_NAME,
  COL2_COUNT
};

static ClutterColor bike_color = {0xaa, 0x44, 0x44, 0xee};
static ClutterColor dock_color = {0x44, 0x44, 0xaa, 0xee};

struct _BixiPluginPrivate
{
  GtkWidget *bixi_page;
  GtkWidget *treeview;
  GtkWidget *combobox;
  GtkTreeModel *model;
  GtkTreeViewColumn *extra_column;
  GtkCellRenderer *extra_cell;
  gboolean show_bikes;

  GtkWidget *statusbar;
  guint statusbar_ctx;

  RestProxy *proxy;
  RestProxyCall *call;

  ChamplainView *map_view;
  ChamplainLayer *layer;

  guint timeout_id;
};

static void
present_sidebar (BixiPlugin *plugin)
{
  EmerillonWindow *window;
  EmerillonSidebar *sidebar;

  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  window = EMERILLON_WINDOW (emerillon_window_dup_default ());
  sidebar = EMERILLON_SIDEBAR (emerillon_window_get_sidebar (window));

  emerillon_sidebar_set_page (sidebar, priv->bixi_page);
  gtk_widget_show (GTK_WIDGET (sidebar));

  g_object_unref (window);
}

static gboolean
find_in_model (BixiPlugin *plugin,
               const gchar *id,
               GtkTreeIter *iter,
               ChamplainMarker **marker)
{
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;
  GtkTreeIter i;

  if (!gtk_tree_model_get_iter_first (priv->model, &i))
    return FALSE;

  do
    {
      gchar *iter_id;
      ChamplainMarker *m;

      gtk_tree_model_get (priv->model, &i,
                          COL_ID, &iter_id,
                          COL_MARKER, &m,
                          -1);

      if (strcmp (id, iter_id) == 0)
        {
          *iter = i;
          *marker = m;

          g_free (iter_id);
          g_object_unref (m);

          return TRUE;
        }

      g_free (iter_id);
      g_object_unref (m);
    }
  while (gtk_tree_model_iter_next (priv->model, &i));

  return FALSE;
}

static void
result_cb (RestProxyCall *call,
           GError *error,
           GObject *weak_object,
           BixiPlugin *plugin)
{
  const gchar *answer;
  gint len;
  RestXmlParser *parser;
  RestXmlNode *root, *n;
  gfloat min_lat, max_lat, min_lon, max_lon;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  answer = rest_proxy_call_get_payload (call);
  len = rest_proxy_call_get_payload_length (call);
  parser = rest_xml_parser_new ();

  root = rest_xml_parser_parse_from_data (parser, answer, len);

  n = rest_xml_node_find (root, "station");

  while (n)
    {
      RestXmlNode *id, *name, *terminal_name, *installed, *locked, *temporary,
                  *nb_bikes, *nb_empty_docks, *lon, *lat;
      GtkTreeIter iter;
      ChamplainMarker *marker;
      gfloat flon, flat;
      gboolean binstalled, blocked, btemporary;
      gint inb_bikes, inb_empty_docks;

      id = rest_xml_node_find (n, "id");
      if (!id)
        {
          n = n->next;
          continue;
        }

      name = rest_xml_node_find (n, "name");
      if (!name)
        {
          n = n->next;
          continue;
        }

      terminal_name = rest_xml_node_find (n, "terminalName");
      if (!terminal_name)
        {
          n = n->next;
          continue;
        }

      lat = rest_xml_node_find (n, "lat");
      if (!lat)
        {
          n = n->next;
          continue;
        }
      flat = g_strtod (lat->content, NULL);
      /* Some debug stations have invalid latitude */
      if (flat == 999)
        {
          n = n->next;
          continue;
        }

      lon = rest_xml_node_find (n, "long");
      if (!lon)
        {
          n = n->next;
          continue;
        }
      flon = g_strtod (lon->content, NULL);

      installed = rest_xml_node_find (n, "installed");
      if (!installed)
        {
          n = n->next;
          continue;
        }
      binstalled = (strcmp (installed->content, "true") == 0);
      if (!binstalled)
        {
          n = n->next;
          continue;
        }

      locked = rest_xml_node_find (n, "locked");
      if (!locked)
        {
          n = n->next;
          continue;
        }
      blocked = (strcmp (locked->content, "true") == 0);
      if (blocked)
        {
          n = n->next;
          continue;
        }

      temporary = rest_xml_node_find (n, "temporary");
      if (!temporary)
        {
          n = n->next;
          continue;
        }
      btemporary = (strcmp (temporary->content, "true") == 0);

      nb_bikes = rest_xml_node_find (n, "nbBikes");
      if (!nb_bikes)
        {
          n = n->next;
          continue;
        }
      inb_bikes = g_strtod (nb_bikes->content, NULL);

      nb_empty_docks = rest_xml_node_find (n, "nbEmptyDocks");
      if (!nb_empty_docks)
        {
          n = n->next;
          continue;
        }
      inb_empty_docks = g_strtod (nb_empty_docks->content, NULL);

      /* Create the marker */
      if (!find_in_model (plugin, id->content, &iter, &marker))
        {
          marker = CHAMPLAIN_MARKER (bixi_marker_new ());
          gtk_list_store_append (GTK_LIST_STORE (priv->model), &iter);
          clutter_container_add_actor (CLUTTER_CONTAINER (priv->layer),
                                       CLUTTER_ACTOR (marker));

          clutter_actor_show (CLUTTER_ACTOR (marker));
        }

      champlain_base_marker_set_position (CHAMPLAIN_BASE_MARKER (marker),
                                          flat,
                                          flon);

      if (priv->show_bikes)
        {
          bixi_marker_set_value (BIXI_MARKER (marker), inb_bikes);
          champlain_marker_set_color (marker, &bike_color);
        }
      else
        {
          bixi_marker_set_value (BIXI_MARKER (marker), inb_empty_docks);
          champlain_marker_set_color (marker, &dock_color);
        }

      /* Create the row item */
      gtk_list_store_set (GTK_LIST_STORE (priv->model), &iter,
                          COL_ID, id->content,
                          COL_NAME, name->content,
                          COL_TERMINAL_NAME, terminal_name->content,
                          COL_LAT, flat,
                          COL_LON, flon,
                          COL_INSTALLED, binstalled,
                          COL_LOCKED, blocked,
                          COL_TEMPORARY, btemporary,
                          COL_NB_BIKES, inb_bikes,
                          COL_NB_BIKES_S, nb_bikes->content,
                          COL_NB_EMPTY_DOCKS, inb_empty_docks,
                          COL_NB_EMPTY_DOCKS_S, nb_empty_docks->content,
                          COL_MARKER, marker,
                          -1);

      n = n->next;
    }

  rest_xml_node_unref (root);

  gtk_statusbar_pop (GTK_STATUSBAR (priv->statusbar),
                     priv->statusbar_ctx);
}


static gboolean
bixi_load_data (BixiPlugin *plugin)
{
  GError *error = NULL;
  GList *children, *l;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  gtk_statusbar_push (GTK_STATUSBAR (priv->statusbar),
                      priv->statusbar_ctx,
                      g_dgettext (GETTEXT_PACKAGE, "Loading Montreal Public Bike System data from bixi.com..."));

  if (priv->proxy == NULL)
    priv->proxy = rest_proxy_new ("https://profil.bixi.ca/data/", FALSE);

  /* Cancel previous call */
  if (priv->call)
    g_object_unref (priv->call);
  priv->call = rest_proxy_new_call (priv->proxy);

  rest_proxy_set_user_agent (priv->proxy, "Emerillon/"VERSION);
  rest_proxy_call_set_function (priv->call, "bikeStations.xml");
  rest_proxy_call_set_method (priv->call, "GET");

  if (!rest_proxy_call_async (priv->call,
        (RestProxyCallAsyncCallback) result_cb,
        G_OBJECT (priv->proxy),
        plugin,
        &error))
    {
      g_error ("Cannot make call: %s", error->message);
      g_error_free (error);
    }

  return TRUE;
}

static void
bixi_activate_cb (GtkEntry *entry,
                    BixiPlugin *plugin)
{
  bixi_address (plugin);
}

static void
bixi_icon_activate_cb (GtkEntry *entry,
                         GtkEntryIconPosition position,
                         GdkEvent *event,
                         BixiPlugin *plugin)
{
  bixi_address (plugin);
}

#if CHAMPLAIN_CHECK_VERSION(0, 4, 1)
static void
marker_selected_cb (ChamplainSelectionLayer *layer,
                    BixiPlugin *plugin)
{
  GtkTreeIter iter;
  ChamplainBaseMarker *selected;
  GtkTreeSelection *selection;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  selected = champlain_selection_layer_get_selected (layer);

  if (!selected)
    return;

  if (!gtk_tree_model_get_iter_first (priv->model, &iter))
    return;

  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->treeview));

  do
    {
      ChamplainBaseMarker *marker;
      gtk_tree_model_get (priv->model, &iter, COL_MARKER, &marker, -1);

      if (!marker)
        continue;

      if (marker == selected)
        {
          GtkTreePath *path;

          gtk_tree_selection_select_iter (selection, &iter);
          path = gtk_tree_model_get_path (priv->model, &iter);
          gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (priv->treeview), path, NULL, FALSE, 0, 0);

          present_sidebar (plugin);

          g_object_unref (marker);
          gtk_tree_path_free (path);
          return;
        }

      g_object_unref (marker);
    }
  while (gtk_tree_model_iter_next (priv->model, &iter));
}
#endif

static void
row_selected_cb (GtkTreeSelection *selection,
                 BixiPlugin *plugin)
{
  GtkTreeIter iter;
  ChamplainBaseMarker *marker;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  if (!gtk_tree_selection_get_selected (selection, &priv->model, &iter))
    return;

  gtk_tree_model_get (priv->model, &iter, COL_MARKER, &marker, -1);

  if (!marker)
    return;

  champlain_selection_layer_select (CHAMPLAIN_SELECTION_LAYER (priv->layer),
        marker);

  g_object_unref (marker);
}

static void
row_activated_cb (GtkTreeView *tree_view,
                  GtkTreePath *path,
                  GtkTreeViewColumn *column,
                  BixiPlugin *plugin)
{
  GtkTreeIter iter;
  gfloat lat, lon;
  ChamplainMarker *marker;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  if (!gtk_tree_model_get_iter (priv->model, &iter, path))
    return;

  gtk_tree_model_get (priv->model, &iter, COL_MARKER, &marker, -1);

  if (!marker)
    return;

  gtk_tree_model_get (priv->model, &iter, COL_LAT, &lat, COL_LON, &lon, -1);

  if (champlain_view_get_zoom_level (priv->map_view) < 16)
    champlain_view_set_zoom_level (priv->map_view, 16);

  champlain_view_center_on (priv->map_view, lat, lon);
  g_object_unref (marker);
}

static gboolean
select_function_cb (GtkTreeSelection *selection,
                    GtkTreeModel *model,
                    GtkTreePath *path,
                    gboolean path_currently_selected,
                    BixiPlugin *plugin)
{
  GtkTreeIter iter;
  GValue value = {0};
  ChamplainBaseMarker *marker;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  if (path_currently_selected)
    return TRUE;

  if (!gtk_tree_model_get_iter (priv->model, &iter, path))
    return FALSE;

  gtk_tree_model_get_value (priv->model, &iter, COL_MARKER, &value);
  marker = g_value_get_object (&value);
  g_value_unset (&value);

  return marker != NULL;
}

static void
view_changed_cb (GtkComboBox *combo,
                 BixiPlugin *plugin)
{
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;
  GtkTreeIter iter;
  ClutterColor *color;

  priv->show_bikes = (gtk_combo_box_get_active (combo) == 0);

  if (!priv->model)
    return;

  if (priv->show_bikes)
    {
      g_object_set (priv->extra_column, "title", g_dgettext (GETTEXT_PACKAGE, "Bikes"), NULL);
      gtk_tree_view_column_set_sort_column_id (priv->extra_column, COL_NB_BIKES);
      gtk_tree_view_column_set_attributes (priv->extra_column,
                                           priv->extra_cell,
                                           "text", COL_NB_BIKES_S,
                                           NULL);
      color = &bike_color;
    }
  else
    {
      g_object_set (priv->extra_column, "title", g_dgettext (GETTEXT_PACKAGE, "Docks"), NULL);
      gtk_tree_view_column_set_sort_column_id (priv->extra_column, COL_NB_EMPTY_DOCKS);
      gtk_tree_view_column_set_attributes (priv->extra_column,
                                           priv->extra_cell,
                                           "text", COL_NB_EMPTY_DOCKS_S,
                                           NULL);
      color = &dock_color;
    }

  gtk_tree_model_get_iter_first (priv->model, &iter);

  do
    {
      ChamplainMarker *marker;
      guint bikes, docks, value;

      gtk_tree_model_get (priv->model, &iter,
                          COL_MARKER, &marker,
                          COL_NB_BIKES, &bikes,
                          COL_NB_EMPTY_DOCKS, &docks,
                          -1);
      champlain_marker_set_color (marker, color);
      if (priv->show_bikes)
        value = bikes;
      else
        value = docks;
      bixi_marker_set_value (BIXI_MARKER (marker), value);

      g_object_unref (marker);
    }
  while (gtk_tree_model_iter_next (priv->model, &iter));

}

static void
build_combo_box (BixiPlugin *plugin)
{
  GtkListStore *store;
  GtkTreeModel *model;
  GtkCellRenderer *cell;
  GtkTreeViewColumn *column;
  GtkTreeSelection *selection;
  GtkWidget *label, *box;
  GtkTreeIter iter;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  box = gtk_hbox_new (FALSE, 0);
  label = gtk_label_new (g_dgettext (GETTEXT_PACKAGE, "Show"));
  gtk_misc_set_padding (GTK_MISC (label), 10, 0);
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (priv->bixi_page), GTK_WIDGET (box), FALSE, FALSE, 0);

  /* Setup result combobox */
  store = gtk_list_store_new (COL2_COUNT,
                              G_TYPE_STRING,       /* Icon */
                              G_TYPE_STRING);      /* Name */

  model = GTK_TREE_MODEL (store);

  priv->combobox = gtk_combo_box_new_with_model (model);
  gtk_box_pack_start (GTK_BOX (box), priv->combobox, TRUE, TRUE, 0);
  g_signal_connect (priv->combobox, "changed",
      G_CALLBACK (view_changed_cb),
      plugin);

  cell = gtk_cell_renderer_text_new ();
  g_object_set (cell,
                "ellipsize", PANGO_ELLIPSIZE_END,
                NULL);
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (priv->combobox), cell, TRUE);
  gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (priv->combobox), cell,
                                 "text", COL2_NAME);

  /* Create the row item */
  gtk_list_store_append (GTK_LIST_STORE (model), &iter);
  gtk_list_store_set (GTK_LIST_STORE (model), &iter,
                      COL2_ICON, "",
                      COL2_NAME, g_dgettext (GETTEXT_PACKAGE, "Available Bikes"),
                      -1);

  gtk_list_store_append (GTK_LIST_STORE (model), &iter);
  gtk_list_store_set (GTK_LIST_STORE (model), &iter,
                      COL2_ICON, "",
                      COL2_NAME, g_dgettext (GETTEXT_PACKAGE, "Empty Docks"),
                      -1);
  gtk_combo_box_set_active (GTK_COMBO_BOX (priv->combobox), 0);

  gtk_widget_show_all (box);
}

static void
build_tree_view (BixiPlugin *plugin)
{
  GtkListStore *store;
  GtkCellRenderer *cell;
  GtkTreeViewColumn *column;
  GtkTreeSelection *selection;
  GtkWidget *scrolled;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  /* Setup result treeview */
  store = gtk_list_store_new (COL_COUNT,
                              G_TYPE_STRING,       /* Id */
                              G_TYPE_STRING,       /* Name */
                              G_TYPE_STRING,       /* Terminal Name */
                              G_TYPE_FLOAT,        /* Latitude */
                              G_TYPE_FLOAT,        /* Longitude */
                              G_TYPE_BOOLEAN,      /* Installed */
                              G_TYPE_BOOLEAN,      /* Locked */
                              G_TYPE_BOOLEAN,      /* Temporary */
                              G_TYPE_UINT,         /* Nb bikes */
                              G_TYPE_STRING,       /* Nb bikes as string */
                              G_TYPE_UINT,         /* Nb empty docks */
                              G_TYPE_STRING,       /* Nb empty docks as string */
                              G_TYPE_OBJECT);      /* Marker */

  priv->model = GTK_TREE_MODEL (store);

  priv->treeview = gtk_tree_view_new ();
  g_signal_connect (priv->treeview, "row-activated",
      G_CALLBACK (row_activated_cb),
      plugin);
  selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->treeview));
  gtk_tree_selection_set_select_function (selection,
      (GtkTreeSelectionFunc) select_function_cb, plugin, NULL);
  g_signal_connect (selection, "changed",
      G_CALLBACK (row_selected_cb),
      plugin);
  gtk_tree_view_set_model (GTK_TREE_VIEW (priv->treeview), priv->model);
  gtk_tree_view_set_search_column (GTK_TREE_VIEW (priv->treeview), COL_NAME);
  gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (priv->model),
                                        COL_NAME,
                                        GTK_SORT_ASCENDING);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
      GTK_POLICY_AUTOMATIC,
      GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled),
      GTK_SHADOW_IN);
  gtk_container_add (GTK_CONTAINER (scrolled), priv->treeview);
  gtk_box_pack_start (GTK_BOX (priv->bixi_page), scrolled, TRUE, TRUE, 0);
  gtk_widget_show_all (scrolled);

  cell = gtk_cell_renderer_text_new ();
  g_object_set (cell,
                "ellipsize", PANGO_ELLIPSIZE_END,
                NULL);

  column = gtk_tree_view_column_new_with_attributes (g_dgettext (GETTEXT_PACKAGE, "Name"),
                                                     cell,
                                                     "text", COL_NAME,
                                                     NULL);

  gtk_tree_view_column_set_sort_column_id (column, COL_NAME);
  gtk_tree_view_column_set_expand (column, TRUE);
  gtk_tree_view_append_column (GTK_TREE_VIEW (priv->treeview), column);

  priv->extra_cell = gtk_cell_renderer_text_new ();
  g_object_set (priv->extra_cell,
                "alignment", PANGO_ALIGN_RIGHT,
                NULL);
  gtk_cell_renderer_set_alignment (GTK_CELL_RENDERER (priv->extra_cell), 1.0, 0.5);
  priv->extra_column = gtk_tree_view_column_new_with_attributes (g_dgettext (GETTEXT_PACKAGE, "Bikes"),
                                                     priv->extra_cell,
                                                     "text", COL_NB_BIKES_S,
                                                     NULL);

  gtk_tree_view_column_set_sort_column_id (priv->extra_column, COL_NB_BIKES);
  gtk_tree_view_column_set_expand (priv->extra_column, FALSE);
  gtk_tree_view_append_column (GTK_TREE_VIEW (priv->treeview), priv->extra_column);
}

static void
activated (EthosPlugin *plugin)
{
  GtkWidget *window, *sidebar;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  priv->proxy = NULL;
  priv->call = NULL;
  priv->show_bikes = TRUE;
  priv->model = NULL;
  window = emerillon_window_dup_default ();
  sidebar = emerillon_window_get_sidebar (EMERILLON_WINDOW (window));
  priv->map_view = emerillon_window_get_map_view (EMERILLON_WINDOW (window));

  /* Statusbar */
  priv->statusbar = emerillon_window_get_statusbar (EMERILLON_WINDOW (window));
  priv->statusbar_ctx = gtk_statusbar_get_context_id (GTK_STATUSBAR (priv->statusbar),
                                                      "Bixi");

  /* Bixi sidebar page. */
  priv->bixi_page = gtk_vbox_new (FALSE, 5);

  /* FIXME: set this based on the sidebar size. */
  gtk_widget_set_size_request (priv->bixi_page, 200, -1);

  build_combo_box (BIXI_PLUGIN (plugin));
  build_tree_view (BIXI_PLUGIN (plugin));

  emerillon_sidebar_add_page (EMERILLON_SIDEBAR (sidebar),
      g_dgettext (GETTEXT_PACKAGE, "Montreal Public Bike System"), priv->bixi_page);
  gtk_widget_show (priv->bixi_page);

  /* Setup result layer */
  priv->layer = champlain_selection_layer_new();
  champlain_view_add_layer (priv->map_view,
      priv->layer);

#if CHAMPLAIN_CHECK_VERSION(0, 4, 1)
  g_signal_connect (priv->layer,
                    "changed",
                    G_CALLBACK (marker_selected_cb),
                    plugin);
#endif

  clutter_actor_show (CLUTTER_ACTOR (priv->layer));


  g_object_unref (window);

  bixi_load_data (BIXI_PLUGIN (plugin));

  priv->timeout_id = g_timeout_add_seconds (300,
                                            (GSourceFunc) bixi_load_data,
                                            plugin);

}

static void
deactivated (EthosPlugin *plugin)
{
  GtkWidget *window, *sidebar;
  ChamplainView *view;
  BixiPluginPrivate *priv = BIXI_PLUGIN (plugin)->priv;

  g_source_remove (priv->timeout_id);

  if (priv->proxy)
    {
      g_object_unref (priv->proxy);
      priv->proxy = NULL;
    }

  if (priv->call)
    {
      g_object_unref (priv->call);
      priv->call = NULL;
    }

  if (priv->model)
    {
      g_object_unref (priv->model);
      priv->model = NULL;
    }

  window = emerillon_window_dup_default ();
  sidebar = emerillon_window_get_sidebar (EMERILLON_WINDOW (window));
  view = emerillon_window_get_map_view (EMERILLON_WINDOW (window));

#if CHAMPLAIN_CHECK_VERSION(0, 4, 1)
  champlain_view_remove_layer (view, priv->layer);
#endif

  emerillon_sidebar_remove_page (EMERILLON_SIDEBAR (sidebar), priv->bixi_page);
  g_object_unref (window);
}

static void
bixi_plugin_class_init (BixiPluginClass *klass)
{
  EthosPluginClass *plugin_class;

  g_type_class_add_private (klass, sizeof (BixiPluginPrivate));

  plugin_class = ETHOS_PLUGIN_CLASS (klass);
  plugin_class->activated = activated;
  plugin_class->deactivated = deactivated;
}

static void
bixi_plugin_init (BixiPlugin *plugin)
{
  plugin->priv = G_TYPE_INSTANCE_GET_PRIVATE (plugin,
                                              BIXI_TYPE_PLUGIN,
                                              BixiPluginPrivate);
}

EthosPlugin*
bixi_plugin_new (void)
{
  return g_object_new (BIXI_TYPE_PLUGIN, NULL);
}

G_MODULE_EXPORT EthosPlugin*
ethos_plugin_register (void)
{
  return bixi_plugin_new ();
}
