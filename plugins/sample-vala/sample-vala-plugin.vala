/*
 * Copyright (C) 2010 Simon Wenner <simon@wenner.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * Emerillon Vala plugin example.
 *
 * This plugin prints a message when it is loaded and unloaded.
 * Implementing the 'PeasGtk.Configurable' interface is optional. It is only
 * needed if you want a configuration dialog.
 *
 * To compile it run: ./configure --with-plugins=really-all
 */

using GLib;
using Gtk;
using PeasGtk;
using Emerillon;

public class SampleValaPlugin : GLib.Object, Peas.Activatable, PeasGtk.Configurable
{
  public weak GLib.Object object { get; construct; }

  /* A reference to the Emerillon main window */
  private unowned Emerillon.Window window;

  /* The constructor */
  public SampleValaPlugin ()
  {
    /* constructor chain up hint */
    GLib.Object ();
  }

  /* This function gets called when the plugin is loaded.
   * Initilize your plugin and add menu entries or a sidebar.
   */
  public void activate ()
  {
    this.window = Emerillon.Window.dup_default ();

    GLib.message ("SampleValaPlugin activated.");
  }

  /* This functions gets called when the plugin is unloaded.
   * Make sure that you undo all the changes of your plugin in Emerillon.
   */
  public void deactivate ()
  {
    GLib.message ("SampleValaPlugin deactivated.");
  }

  /* This function gets called when the internal state of the application changes.
   * We don't need this function, but we have to provide it.
   */
  public void update_state ()
  {
  }

  /* Implementation of the 'PeasGtk.Configurable' interface.
   * This function creates the widgets inside a small dialog with a close button.
   */
  public Gtk.Widget create_configure_widget ()
  {
    var label = new Gtk.Label ("Welcome to the Vala example configuration.");

    return label;
  }
}

/* Main plugin function. */
[ModuleInit]
public void peas_register_types (GLib.TypeModule module)
{
  var objmodule = module as Peas.ObjectModule;
  objmodule.register_extension_type (typeof(Peas.Activatable), typeof(SampleValaPlugin));
  objmodule.register_extension_type (typeof(PeasGtk.Configurable), typeof(SampleValaPlugin));
}
