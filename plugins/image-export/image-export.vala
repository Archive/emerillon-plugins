/*
 * Copyright (C) 2010 Simon Wenner <simon@wenner.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

using GLib;
using Peas;
using Champlain;
using GConf;

public class ImageExportPlugin : GLib.Object, Peas.Activatable
{
  public weak GLib.Object object { get; construct; }
  private weak Emerillon.Window window;
  private Champlain.View view;
  private Gtk.ActionGroup action_group;
  private uint ui_id;

  public ImageExportPlugin ()
  {
    /* constructor chain up hint */
    GLib.Object ();
  }

  public void activate ()
  {
    this.window = Emerillon.Window.dup_default ();
    this.view = window.get_map_view ();

    /* Add menu entries */
    Gtk.UIManager manager = window.get_ui_manager ();

    this.action_group = new Gtk.ActionGroup ("ImageExportActions");
    action_group.set_translation_domain ("emerillon-plugins");

    Gtk.Action action;
    action =  new Gtk.Action ("ImageExportActionSave",
        _("Export as _image"), "", Gtk.Stock.SAVE_AS);
    action_group.add_action (action);
    action.activate.connect (save_as_image_callback);

    action =  new Gtk.Action ("ImageExportActionMail",
        _("Attach to _email"), "", "");
    action_group.add_action (action);
    action.activate.connect (attach_to_email_callback);

    manager.insert_action_group (action_group, -1);

    string ui = """<ui>
        <menubar name="MainMenu">
            <menu name="Edit" action="Edit">
            <placeholder name="EditPluginMenu">
                <menuitem name="ImageExportItemSave" action="ImageExportActionSave" />
                <menuitem name="ImageExportItemMail" action="ImageExportActionMail" />
            </placeholder>
            </menu>
        </menubar>
    </ui>""";

    try {
      ui_id = manager.add_ui_from_string (ui, ui.length);
    }
    catch (GLib.Error e) {
      GLib.error ("%s", e.message);
    }
  }

  public void deactivate ()
  {
    /* remove the buttons */
    Gtk.UIManager manager = window.get_ui_manager ();
    manager.remove_ui (ui_id);
    manager.remove_action_group (action_group);
  }

  public void update_state ()
  {
  }

  private void grab_and_save_image (string path) throws GLib.Error
  {
    /* create an image of the stage */
    var clutter_stage = view.get_stage ();
    uchar[] pixels = clutter_stage.read_pixels (0, 0, -1, -1);
    int width = (int) clutter_stage.width;
    int height = (int) clutter_stage.height;

    var pixbuf = new Gdk.Pixbuf.from_data (pixels, Gdk.Colorspace.RGB,
        true, 8, width, height, width * 4, null);
    /* save it as png file */
    pixbuf.save (path, "png", null, null);
  }

  private void save_as_image_callback (Gtk.Action action)
  {
    /* create the save dialog */
    var file_chooser = new Gtk.FileChooserDialog (_("Save as png image"),
        null, Gtk.FileChooserAction.SAVE, Gtk.Stock.CANCEL,
        Gtk.ResponseType.CANCEL, Gtk.Stock.SAVE, Gtk.ResponseType.ACCEPT,
        null);

    file_chooser.set_current_name (_("map.png"));
    file_chooser.set_do_overwrite_confirmation (true);

    if (file_chooser.run () == Gtk.ResponseType.ACCEPT)
    {
      string path = file_chooser.get_filename ();
      try {
        grab_and_save_image (path);
      }
      catch (GLib.Error e) {
        /* saving failed */
        var dialog = new Gtk.MessageDialog (file_chooser,
                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                Gtk.MessageType.ERROR,
                Gtk.ButtonsType.CLOSE,
                _("Error"));
        dialog.secondary_text = e.message;
        dialog.run ();
        dialog.destroy ();
      }
    }
    file_chooser.destroy ();
  }

  private void attach_to_email_callback (Gtk.Action action)
  {
    /* get the default email application */
    var gc = GConf.Client.get_default ();
    string default_cmd;
    try {
      default_cmd = gc.get_string ("/desktop/gnome/url-handlers/mailto/command");
    } catch (GLib.Error e) {
      GLib.warning (e.message);
      default_cmd = "evolution";
    }

    /* save the image in the temp folder */
    string path = GLib.Environment.get_tmp_dir () + "/" + _("map.png");

    /* Detect the client and set the required command arguments */
    string cmd;
    if (default_cmd.contains ("evolution")) {
      cmd = "evolution mailto:?cc=\\&subject=\"\"\\&attach=\"" + path + "\"";
    } else if (default_cmd.contains ("thunderbird")) {
      cmd = "thunderbird -compose attachment=\"file://" + path + "\"";
    } else if (default_cmd.contains ("icedove")) {
      cmd = "icedove -compose attachment=\"file://" + path + "\"";
    } else if (default_cmd.contains ("mutt")) {
      cmd = "mutt -a \"" + path + "\"";
    } else {
      /* unknown mail client */
      var dialog = new Gtk.MessageDialog (window,
          Gtk.DialogFlags.DESTROY_WITH_PARENT,
          Gtk.MessageType.ERROR,
          Gtk.ButtonsType.CLOSE,
          _("Error"));
      dialog.secondary_text = _("Your email client is not supported.");
      dialog.run ();
      dialog.destroy ();
      return;
    }

    try {
      grab_and_save_image (path);
    }
    catch (GLib.Error e)
    {
      GLib.critical (e.message);
    }

    try {
      GLib.message ("Execute mail command: '%s\'", cmd);
      GLib.Process.spawn_command_line_async (cmd);
    }
    catch (GLib.SpawnError e)
    {
      GLib.critical (e.message);
    }
  }
}

[ModuleInit]
public void peas_register_types (GLib.TypeModule module)
{
  var objmodule = module as Peas.ObjectModule;
  objmodule.register_extension_type (typeof(Peas.Activatable), typeof(ImageExportPlugin));
}
